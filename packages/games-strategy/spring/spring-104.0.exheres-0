# Copyright 2009, 2010 Ingmar Vanhassel
# Copyright 2012, 2013 Wouter van Kesteren <woutershep@gmail.com>
# Distributed under the terms of the GNU General Public License v2

require cmake flag-o-matic freedesktop-desktop freedesktop-mime

SUMMARY="A powerful free RTS engine"
DESCRIPTION="
Spring is a project aiming to create a new and versatile RTS Engine. It features:
* Large battles limited only by the power of your computer; support for up to
  5000 units.
* Large, highly detailed maps in which to wage those battles, fully 3D with
  deformable terrain, forest fires, dynamic and reflective water, and custom
  skyboxes.
* Several camera modes, allowing for anything to be viewed from almost any angle.
* Fully 3D combat in land, sea, and air, with realistic weapon trajectories
  (physics engine).
* Many different Games, made just for Spring.
* Complex 3rd party AIs (Bots) that can work very competently in many cases
  with multiple Spring engine games and mods.
* A powerful GUI designed to minimize unnecessary micromanagement that is
  easily extensible via a Lua API.
* Frequent engine additions and bugfixes.
"
HOMEPAGE="https://springrts.com/"
DOWNLOADS="mirror://sourceforge/springrts/${PN}_${PV}_src.tar.lzma"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS=""

# FIXME internal copies in rts/lib/ >:(
# lua hpiutil2 oscpack streflop squish
DEPENDENCIES="
    build:
        app-arch/p7zip
        app-doc/asciidoc
    build+run:
        dev-lang/python:=
        dev-libs/boost[>=1.35.0]
        media-libs/DevIL
        media-libs/freetype:2
        media-libs/glew[>=1.5.1]
        media-libs/libogg
        media-libs/libvorbis
        media-libs/openal
        media-libs/SDL:2[X]
        sys-libs/zlib
        x11-dri/freeglut
        x11-dri/mesa
        x11-libs/libX11
        x11-libs/libXcursor
"

CMAKE_SOURCE=${WORKBASE}/${PN}_${PV}

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/${PN}-104.0-fix-compile.patch
    "${FILES}"/${PNV}-fix-6358-compile-error-because-of-new-openal-version.patch
    "${FILES}"/${PNV}-close-491.patch
    "${FILES}"/${PN}-Add-missing-include.patch
)

src_prepare() {
    edo pushd "${CMAKE_SOURCE}"

    default

   # lets leave that to build_options: symbols
    edo sed \
        -e 's:-Wl,--compress-debug-sections=zlib::' \
        -i CMakeLists.txt

    edo popd
}

src_configure() {
    # disables UDPListener IPv6 test
    export CI=1

    # FIXME: support Java + Python AI's
    local cmakeparams=(
        -DAI_TYPES=NATIVE
        -DAPPLICATIONS_DIR:STRING=/usr/share/applications
        -DCUSTOM_COMPILER_DEFINITIONS=-DNDEBUG
        -DDATADIR:STRING=/usr/share/${PN}
        -DDOCDIR:STRING=/usr/share/doc/${PNVR}
        -DMANDIR:STRING=/usr/share/man
        -DMARCH_FLAG=$(get-flag march || echo NONE)
        -DMIME_DIR:STRING=/usr/share/mime
        -DPIXMAPS_DIR:STRING=/usr/share/pixmaps
        -DSPRING_DATADIR:PATH=/usr/share/${PN}
        -DUSE_TCMALLOC:BOOL=FALSE
    )

    # wrong cxx_flags will lead to desyncs in multiplayer, the following -O*
    # are all used by upstream in the various build targets and thus safe
    # the -g* shouldn't affect the program in any way so should be safe.
    ALLOWED_FLAGS="-O -O0 -O1 -O2 -g*"
    # spring has a LTO option, so these might be safe. or might not.
    ALLOWED_FLAGS="${ALLOWED_FLAGS} -flto -flto-compression-level -flto-partition -fno-fat-lto-objects -fuse-linker-plugin"
    strip-flags

    ecmake "${cmakeparams[@]}"
}

pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    freedesktop-mime_pkg_postrm
}

pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    freedesktop-mime_pkg_postinst
}

