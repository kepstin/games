# Copyright 2022 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ tag=v${PV} ] cmake
require gtk-icon-cache

SUMMARY="A 2D space trading and combat game similar to the Escape Velocity series"
DESCRIPTION="
Explore other star systems. Earn money by trading, carrying passengers, or
completing missions. Use your earnings to buy a better ship or to upgrade the
weapons and engines on your current one. Blow up pirates. Take sides in a
civil war. Or leave human space behind and hope to find friendly aliens whose
culture is more civilized than your own."

HOMEPAGE="https://endless-sky.github.io/ ${HOMEPAGE}"

LICENCES="
    GPL-3
    CCPL-Attribution-ShareAlike-3.0 [[ note = images ]]
    CCPL-Attribution-ShareAlike-4.0 [[ note = images ]]
    pulibc-domain [[ note = images ]]
"
SLOT="0"
MYOPTIONS="
    opengles [[ description = [ Build the game with OpenGL ES ] ]]
    ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build+run:
        dev-libs/libglvnd
        media-libs/glew
        media-libs/libmad
        media-libs/libpng:=
        media-libs/openal
        media-libs/SDL:2
        sys-apps/util-linux
        providers:ijg-jpeg? ( media-libs/jpeg:= )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
    suggestion:
        games/endless-sky-high-dpi [[
            description = [ Provides a plugins which install high DPI sprites ]
        ]]
"

UPSTREAM_DOCUMENTATION="https://github.com/endless-sky/endless-sky/wiki/PlayersManual"

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DCMAKE_INSTALL_DOCDIR=/usr/share/doc/${PNVR}
    -DCMAKE_INSTALL_MANDIR=/usr/share/man
    -DES_USE_VCPKG:BOOL=FALSE
    -DES_USE_SYSTEM_LIBRARIES:BOOL=TRUE
)
CMAKE_SRC_CONFIGURE_OPTIONS=(
    'opengles ES_GLES'
)
CMAKE_SRC_CONFIGURE_TESTS=(
    '-DBUILD_TESTING:BOOL=TRUE -DBUILD_TESTING:BOOL=FALSE'
)

